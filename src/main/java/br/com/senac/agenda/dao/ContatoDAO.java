/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.agenda.dao;

/**
 *
 * @author sala302b
 */
import br.com.senac.agenda.model.Contato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ContatoDAO extends DAO<Contato> {

    @Override
    public void salvar(Contato contato) {

        Connection connection = null;

        try {

            String query;

            if (contato.getId() == 0) {
                query = "INSERT INTO contato (nome , telefone, celular, endereco, cep, numero, bairro, cidade, estado, email) values (? , ? , ? , ? , ? , ? , ? ,? , ? , ? );";

            } else {
                query = "update contato set nome = ? , telefone = ? , celular = ? , endereco = ? , cep = ? , numero = ? , bairro = ? , cidade = ? , estado = ? , email = ? where id = ? ;";
            }
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, contato.getNome());
            ps.setString(2, contato.getTelefone());
            ps.setString(3, contato.getCelular());
            ps.setString(5, contato.getEndereco());
            ps.setString(4, contato.getCep());
            ps.setString(6, contato.getNumero());
            ps.setString(7, contato.getBairro());
            ps.setString(8, contato.getCidade());
            ps.setString(9, contato.getEstado());
            ps.setString(10, contato.getEmail());

            if (contato.getId() == 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                contato.setId(rs.getInt(1));
            } else {
                ps.setInt(11, contato.getId());
                ps.executeUpdate();
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Falha .......................");
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println("Erro de Conexao");
            }
        }

    }

    @Override
    public void deletar(Contato objeto) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Contato> listar() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Contato get(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Contato> getByFiltro(Integer id, String nome, String estado) {
        List <Contato> lista = new ArrayList();
        Connection connection = null;
        try {
            StringBuilder sb = new StringBuilder("select * from contato where 1 = 1");
            if (id != null) {
                sb.append(" and id = ? ");
            }
            if (nome != null && !nome.trim().isEmpty()) {
                sb.append(" and nome like ? ");
            }
            if (estado != null && !estado.isEmpty()){
                sb.append(" and estado = ? ");
            }
            
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(sb.toString());
            int index = 0;
            if (id != null) {
                ps.setInt(++index, id);
            }
            if (nome != null && !nome.trim().isEmpty()) {
                ps.setString(++index, "%" + nome + "%");
            }
            if (estado != null && !estado.isEmpty()){
                ps.setString(++index, estado);
            }
            
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Contato contato = new Contato();
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                contato.setEndereco(rs.getString("endereco"));
                contato.setTelefone(rs.getString("telefone"));
                contato.setCelular(rs.getString("celular"));
                contato.setEmail(rs.getString("email"));
                contato.setCep(rs.getString("cep"));
                contato.setNumero(rs.getString("numero"));
                contato.setBairro(rs.getString("bairro"));
                contato.setCidade(rs.getString("cidade"));
                contato.setEstado(rs.getString("estado"));
                lista.add(contato);
            }
        } catch (Exception e) {
        System.out.println("Erro ao realizar consulta");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao....");
            }
        }
        
        
        
        return lista;
    }

}
