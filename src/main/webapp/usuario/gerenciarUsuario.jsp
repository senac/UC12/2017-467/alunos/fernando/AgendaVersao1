<%@page import="br.com.senac.agenda.model.Usuario"%>
<jsp:include page="../header.jsp" />
<% 
    Usuario usuario = (Usuario) request.getAttribute("usuario");
    String salvar = (String) request.getAttribute("salvar");
    String erro = (String) request.getAttribute("erro");

%>


<% if (salvar != null) {%>
<div class="alert alert-success">
    <%= salvar%>
</div>
<% } %>

<% if (erro != null) {%>
<div class="alert alert-danger" role="alert">
    <%= erro%>
</div>
<%}%>

<fieldset>
    
<legend>Pesquisa de Usu�rio</legend>
<form  action="./SalvarUsuarioServlet" method="post">
        <div class="form-group">
            <label for="txtCodigo" style="margin-right: 10px">C�digo</label> 
            <input name="id" class="form-control col-1" id="txtCodigo" type="text" readonly value='<%= usuario != null ? usuario.getId() : ""  %>' />

        </div>
        <div class="form-group">
            <label for="txtNome" style="margin-right: 10px">Nome</label> <input id="id" name="nome" class="form-control form-control-sm" id="txtNome" type="text" value='<%= usuario != null ? usuario.getNome() : "" %>'>
        </div>
        <div class="form-group">
            <label for="txtSenha" style="margin-right: 10px">Senha</label> <input name="senha" class="form-control form-control-sm" id="txtSenha" type="text" />
        </div>
        <div>
        <button type="submit" class="btn btn-success" style="margin-left: 10px">Salvar</button>
        <button type="reset" class="btn btn-danger" style="margin-left: 10px">Cancelar</button>
        </div>

    </form>

</fieldset>



<jsp:include page="../footer.jsp" />
