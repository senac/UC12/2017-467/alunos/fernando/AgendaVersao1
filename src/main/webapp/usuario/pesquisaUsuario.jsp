<%@page import="br.com.senac.agenda.model.Usuario"%>
<%@page import="java.util.List"%>
<jsp:include page="../header.jsp" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<script type="text/javascript">

    function  deletar(id, valor) {
        $('#usuario').text(valor);
        
        var formNome = $('#nome').val();
        var formId = $('#id').val();
        var url = './DeletarUsuarioServlet?id=' + id + '&formNome=' + formNome + '&formId' + formId;
        $('#btnDeletar').attr('href' , url);
        
    }
</script>

<% List<Usuario> lista = (List) request.getAttribute("lista"); %>
<% String mensagem = (String) request.getAttribute("mensagem"); %>


<% if (mensagem != null) {%>
<div class="alert alert-danger">
    <%= mensagem%>
</div>
<% } %>

<fieldset  >
    <legend>Pesquisa de Usu�rios</legend>
    <form class="form-inline" action="./PesquisaUsuarioServlet">
        <div class="form-group" style="padding: 20px;">
            <label for="txtCodigo" style="margin-right: 10px">C�digo:</label> 
            <input id="id" name="id" class="form-control form-control-sm" id="txtCodigo" type="text" />
        </div>
        <div class="form-group">
            <label for="nome" style="margin-right: 10px">Nome:</label> 
            <input name="nome" id="nome" class="form-control form-control-sm" type="text" />
        </div>
        <button style="margin-left: 10px" type="submit" class="btn btn-default" >
            <i class="fa fa-search" aria-hidden="true"></i> Pesquisar
        </button>
        <a href="./SalvarUsuarioServlet" style="margin-left: 10px" class="btn btn-success">
            <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
            <a/>
    </form>
</fieldset>
<hr />

<table class="table table-hover">
    <thead>
        <tr>
            <th>C�digo</th> <th>Nome</th> <th></th>
        </tr>
    </thead>

    <% if (lista != null && lista.size() > 0) {
            for (Usuario u : lista) {
    %>
    <tr>
        <td><%= u.getId()%></td><td><%= u.getNome()%></td>
        <td style="width: 120px">

            <a href="./SalvarUsuarioServlet?id=<%= u.getId()%>" style="margin-right: 10px" >
                <img src="../resources/imagens/user-man-edit-icon.png" />
            </a>

            <a href="" data-toggle="modal" data-target="#exampleModal" onclick="deletar (<%= u.getId()%>, '<%= u.getNome()%>');">
                <img src="../resources/imagens/user-man-remove-icon.png" />
            </a>

        </td>
    </tr>

    <% } // for

    } else {%>

    <tr >
        <td  colspan="2">N�o Existem registros.</td>
    </tr>

    <%}%>


</table>
    
    
    <a data-toggle="modal" data-target="#exampleModal">
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirma��o</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Tem certeza que deseja excluir o usuario <span id="usuario"></span>?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">N�o</button>
                    <a id="btnDeletar" href="" class="btn btn-success">Sim</a>
                </div>
            </div>
        </div>
    </div>
    </a>
<jsp:include page="../footer.jsp" />
