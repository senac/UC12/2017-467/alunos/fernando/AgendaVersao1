<%@page import="br.com.senac.agenda.model.Contato"%>
<jsp:include page="../header.jsp"/>

<% 
    Contato usuario = (Contato) request.getAttribute("usuario");
    String salvar = (String) request.getAttribute("salvar");
    String erro = (String) request.getAttribute("erro");

%>


<% if (salvar != null) {%>
<div class="alert alert-success">
    <%= salvar%>
</div>
<% } %>

<% if (erro != null) {%>
<div class="alert alert-danger" role="alert">
    <%= erro%>
</div>
<%}%>


<form action="./CadastroUsuarioServlet" method="post">
    <div class="form-group">
        <label for="txtCodigo" style="margin-right: 10px">C�digo:</label>
        <p><input name="id" type="text" id="codigo" class="form-control col-1" readonly=""/></p>
    </div>

    <hr/>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="nome">Nome Completo:</label>
            <input name= "nome" type="text" class="form-control" id="nome" >
        </div>  
    </div>

    <div class="form-row">
        <div class="form-group col-6">
            <label for="telefone">Telefone:</label>
            <input name="telefone" type="text" class="form-control" id="telefone" >
        </div>

        <div class="form-group col-md-6">
            <label for="celular">Celular:</label>
            <input name="celular" type="text" class="form-control" id="celular" >
        </div>
    </div>  

    <div class="form-row">
        <div class="form-group col-6">
            <label for="endereco">Endere�o:</label>
            <input name="endereco"   type="text" class="form-control" id="endereco" >
        </div>

        <div class="form-group col-4">
            <label for="cep">CEP:</label>
            <input name="cep" type="text" class="form-control" id="cep" >
        </div>

        <div class="form-group col-2">
            <label for="numero">Numero:</label>
            <input name="numero" type="text" class="form-control" id="numero" >
        </div>
    </div>
    <div class="form-row">

        <div class="form-group col-4">
            <label for="bairro">Bairro:</label>
            <input name="bairro"  type="text" class="form-control" id="bairro" >      
        </div>

        <div class="form-group col-4">
            <label for="cidade">Cidade:</label>
            <input name="cidade"  type="text" class="form-control" id="cidade" >      
        </div>

        <div class="form-group col-4">
            <label for="estado">Estado:</label>
            <select type="text" id="estado" name="estado" class="form-control">
                <option selected>UF</option>
                <option>ES</option>
                <option>SP</option>
                <option>RJ</option>
                <option>MG</option>
                <option>BH</option>
            </select>
        </div> 
    </div>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="email">Email:</label>
            <input name="email" type="text" class="form-control" id="email" >
        </div>  
    </div>
    <hr/>

    <div>
        <button type="submit"  class="btn btn-success">Salvar</button>
        <button type="reset"  class="btn btn-danger">Cancelar</button>
    </div>
</form>

<jsp:include page="../footer.jsp"/>
