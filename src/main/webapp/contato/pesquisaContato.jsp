<%@page import="java.util.List"%>
<%@page import="br.com.senac.agenda.model.Contato"%>
<jsp:include page="../header.jsp"/>
<% List<Contato> lista = (List) request.getAttribute("lista"); %>
<% String mensagem = (String) request.getAttribute("mensagem"); %>


<% if (mensagem != null) {%>
<div class="alert alert-danger">
    <%= mensagem%>
</div>
<% } %>

<fieldset  >
    <legend>Pesquisa de Usu�rios</legend>
    <form class="form-inline" action="./PesquisaContatoServlet">
        <div class="form-group" style="padding: 20px;">
            <label for="txtCodigo" style="margin-right: 10px">C�digo:</label> 
            <input name="id" class="form-control form-control-sm" id="txtCodigo" type="text" />
        </div>
        <div class="form-group">
            <label for="nome" style="margin-right: 10px">Nome:</label> 
            <input name="nome" id="nome" class="form-control form-control-sm" type="text" />
        </div>
        <div class="form-group col-4">
            <label for="estado" style="margin-right: 10px">Estado:</label>
            <select type="text" id="estado" name="estado" class="form-control">
                <option value="" selected>--</option>
                <option value="ES">ES</option>
                <option value="SP">SP</option>
                <option value="RJ">RJ</option>
                <option value="MG">MG</option>
                <option value="BH">BH</option>
                <option value="UF">UF</option>
            </select>
        </div>
        <button style="margin-left: 10px" type="submit" class="btn btn-default" >
            Pesquisar
        </button>
    </form>
</fieldset>
<hr />

<table class="table table-hover">
    <thead>
        <tr>
            <th>C�digo</th> <th>Nome</th> <th>Endere�o</th> <th>Telefone</th> <th>Celular</th> <th>E-mail</th>
        </tr>
    </thead>

    <% if (lista != null && lista.size() > 0) {  
        for( Contato u : lista  ){
    %>
        <tr>
            <td><%= u.getId() %></td><td><%= u.getNome() %></td><td><%= u.getEnderecoCompleto()%></td> <td><%= u.getTelefone() %></td> <td><%= u.getCelular() %></td> <td><%= u.getEmail() %></td>
        </tr>

    <% } // for

    }else{%>
      
    <tr >
        <td  colspan="2">N�o Existem registros.</td>
    </tr>
    
    <%}%>


</table>






<jsp:include page="../footer.jsp"/>